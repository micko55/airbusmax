let timeEnd = 0;
let timeBegin = 0;
let flights = [];
let registeredFlights = [];

function buttonClickGET() {
    const date = document.getElementById("date").value;
    const hour = document.getElementById("hour").value;
    const actualDate =  Date.parse(date + "T" + hour)
    timeEnd = Math.floor(actualDate / 1000);
    timeBegin = Math.floor(actualDate / 1000 - .2*60*60)
    const url = "https://opensky-network.org/api/flights/all?begin=" + timeBegin + "&end=" + timeEnd;
    const url2 = "https://opensky-network.org/api/states/all?time=:time:&icao24=:icao24:";
    console.log(url)
    $.get(url, callBackGetSuccess)
    .then(() => {
        console.log("first callback ok! ", flights)
        flights.forEach(element => {
            $.get(url2.replace(":time:", element.lastSeen).replace(":icao24:", element.icao24), (e)=>{callBackGetSuccess2(e, element)})
            .catch(e => { console.log(e) })
        })
    })
    .catch(e => { console.log(e) })
}


function callBackGetSuccess(data) {
    console.log("données api :", data);
    data.forEach(element => {
        flights.push(element)
    });
}

function saveFlight(value){
    registeredFlights.push(value);
    /* const flightsDiv = document.getElementById("flightsDiv");
    const planesDiv = document.getElementById("planesDiv"); */
    const values = value.split(":");
    /* flightsDiv.innerHTML += "icao24: " + values[0] + "<br/>Avion: " + values[1] + 
    "<br/>AeroportDepart: " + values[2] + "<br/>AeroportArrivee: " + values[3] + "<br/>Pays: " + values[4] + "<br/>";
    planesDiv.innerHTML += "id: "+ values[1] + "<br/>Vol: " + values[0] + "<br/>"; */
    //$.get("scripts/function.php"); 

    //document.write("<?php sqlSaveFlight("+value+") ?>");
    $('#arrayVols').remove();
    $('#arrayAvions').remove();
    $.ajax({
        type: "POST",
        url: 'http://localhost:8888/PHP/airbusmax/scripts/function.php',
        data: {parameters: values},
        success: function(data){
            //console.log(data);
            document.getElementById("sql_div").innerHTML += data;
        }
    }); 
}

function callBackGetSuccess2(data, flight) {

    const mainDiv = document.getElementById("main_div");
    console.log("données api2 :", data);
    if (data.states !== null) {
        const lon = data.states[0][5]
        const lat = data.states[0][6]
        if (lat !== null && lon !== null) {
                
            const time = data.states[0][4] === null ? timeEnd : data.states[0][4] // last_contact
            const newDate = new Date(time*1000)
            const strTime = newDate.toLocaleDateString() + " " + newDate.toLocaleTimeString();
            const icao24 = data.states[0][0];
            let string = "";

            string += "icao24: " + icao24 + " lon: " + lon + " lat: " + lat + " last contact: " + strTime; 
            const urlWeather = "https://api.openweathermap.org/data/2.5/onecall?lat=:lat:&lon=:lon:&dt=:time:&units=metric&appid=9813d55f679f47adf46812cd8371840c"
            const weather = urlWeather.replace(":lat:", lat).replace(":lon:", lon).replace(":time:", time)
            $.get(weather, el => {
                string += " Temps: " + el.current.weather[0].description
                console.log(el)
            })
            .then(() => {
                mainDiv.innerHTML += string+" ";
                const btn = document.createElement("button");
                btn.innerHTML = "Sauvegarder ce vol";
                btn.value = icao24+":"+data.states[0][1]+":"+flight.estDepartureAirport+":"+flight.estArrivalAirport+":"+data.states[0][2];
                btn.setAttribute("onclick", "saveFlight(value)");
                btn.setAttribute("name", "saveFlight");
                mainDiv.appendChild(btn);
                console.log(btn);
                mainDiv.innerHTML += "<br/>";
            })
            .catch(e => { console.log(e) })
        }
    }
}

/* const timeBegin = Math.floor(Date.now() / 1000 - 72*60*60); // Heure actuelle - 12h
const timeEnd = Math.floor(Date.now() / 1000 - 71.5*60*60); // Heure actuelle - 11h30

 */
