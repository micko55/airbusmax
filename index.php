<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
            crossorigin="anonymous">
    </script>
    <script src="scripts/main.js"></script>
</head>
<body>
    <div>
        <input type="date" id="date" pattern="[0-9]{2}-[0-9]{2}-[0-9]{4}" required />
        <input type="time" id="hour" required />
        <button onclick="buttonClickGET()">Rechercher</button>
    </div>
    <div id="main_div" style="width:45%; display:inline-block;" method="POST"><p></p></div>
    <div id="sql_div" style="width:45%; display:inline-block;">
        <h1>Bases de données MySQL</h1> 
        <form method="POST">
            <div id="flightsDiv" style="width:65%;display:inline-block;"></div>
            <div id="planesDiv" style="width:32%;display:inline-block;"></div>
        </form>
        <?php
            $servername = 'localhost:8889';
            $dbname = "airbusmax";
            $user = 'root';
            $pass = 'root';
            
            try{
                $dbco = new PDO("mysql:host=$servername;dbname=$dbname", $user, $pass);
                $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $e){
                $dbco = null;
                $dbco = new PDO("mysql:host=$servername", $user, $pass);
                $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $sql = "CREATE DATABASE airbusmax";
                $dbco->exec($sql);
                $dbco = null;
                $dbco = new PDO("mysql:host=$servername;dbname=$dbname", $user, $pass);
                $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            finally {
                try{
                    $sql = "CREATE TABLE vols(
                        icao24 VARCHAR(6),
                        Avion VARCHAR(8),
                        AeroportDepart VARCHAR(4),
                        AeroportArrivee VARCHAR(4),
                        Pays VARCHAR(30),
                        PRIMARY KEY (icao24)
                    );
                    CREATE TABLE avions(
                        id VARCHAR(8),
                        Vol VARCHAR(6),
                        PRIMARY KEY (id),
                        FOREIGN KEY (Vol)
                            REFERENCES vols(icao24)
                            ON DELETE CASCADE
                    );";
                    $dbco->exec($sql);
                }
                finally{
                    $sql = $dbco->prepare("SELECT * FROM vols");
                    $sql->execute();
                    $resultat = $sql->fetchAll(PDO::FETCH_ASSOC);
                    echo '<pre id="arrayVols">';
                    print_r($resultat);
                    echo '</pre>';
                    $sql = $dbco->prepare("SELECT * FROM avions");
                    $sql->execute();
                    $resultat = $sql->fetchAll(PDO::FETCH_ASSOC);
                    echo '<pre id="arrayAvions">';
                    print_r($resultat);
                    echo '</pre>';
                    $dbco = null;
                }
            }

            //include '/scripts/function.php';
        ?>
    </div>

        
</body>
</html>


<!-- Objectif : enregistrer les données météo de certains vols d'avions

Fonctionnalités :
Pouvoir consulter les données de vol
Pouvoir consulter la météo associée à la ville de départ et la ville d'arrivée du vol sélectionné
Pouvoir enregistrer les données météo d'un vol dans une bdd
Pouvoir consulter les météos enregistrées

API OpenSky pour les données de vol : https://opensky-network.org/apidoc/rest.html -->

